#include "LoginRequestHandeler.h"
#include "LoginResponse.h"
#include "LoginRequest.h"
#include "JasonRequestPacketDeserializer.h"
#include "JasonResponsePacketSerializer.h"

#define LOGIN_CODE 1
#define SIGN_UP_CODE 2

JasonRequestPacketDeserializer deserializer; 
JasonResponsePacketSerializer serializer;

LoginRequestHandeler::LoginRequestHandeler(RequestHandelerFactory* RHf, LoginManager* Lmang)
{
	m_loginManager = Lmang;
	m_handelerFactory = RHf;
}

RequestResult LoginRequestHandeler::login(Request req)
{
	RequestResult res;
	LoginResponse resp;
	LoginRequest dsReq = deserializer.deserializeLoginRequest(req.buffer);

	bool success = m_loginManager->login(dsReq.username, dsReq.password);
	resp.status = (int)success;
	if (success)
	{
		res.newHandeler = (IRequestHandeler*)this->m_handelerFactory->createLoginRequestHandeler();
	}
	else
	{
		res.newHandeler = nullptr;
	}
	
	res.response = serializer.serializeResponse(resp);

	return res;
}

RequestResult LoginRequestHandeler::signup(Request req)
{
	RequestResult res;
	SignUpResponse resp;
	SignUpRequest suReq = deserializer.deserializeSignUpRequest(req.buffer);

	m_loginManager->signUp(suReq.username, suReq.password, suReq.email);
	resp.status = 1;
	res.response = serializer.serializeResponse(resp);
	res.newHandeler = nullptr;

	return res;
}

bool LoginRequestHandeler::isRequestRelevant(Request req)
{
	return req.RequestId == LOGIN_CODE || req.RequestId == SIGN_UP_CODE;
}

RequestResult LoginRequestHandeler::handleRequest(Request req)
{
	if (req.RequestId == LOGIN_CODE)
	{
		return login(req);
	}
	return signup(req);
}
