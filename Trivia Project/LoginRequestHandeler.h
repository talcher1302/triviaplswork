#pragma once
#include "LoginManager.h"
#include "IRequestHandeler.h"

class LoginRequestHandeler : public IRequestHandeler
{
private:
	LoginManager* m_loginManager;
	RequestHandelerFactory* m_handelerFactory;

	RequestResult login(Request req);
	RequestResult signup(Request req); 
public:
	LoginRequestHandeler(RequestHandelerFactory* RHf, LoginManager* Lmang);
	bool isRequestRelevant(Request req);
	RequestResult handleRequest(Request req);
};

