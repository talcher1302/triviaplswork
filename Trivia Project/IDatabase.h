#pragma once
#include <iostream>
#include <algorithm>
#include <map>
#include <list>
#include "LoggedUser.h"

class IDatabase
{
public:
	//virtual std::map<LoggedUser, int> getHighScores() = 0;
	virtual bool doesUserExists(std::string userName) = 0;
	//virtual std::list<std::string> getQuestions(int num) = 0;
	virtual void addUser(std::string userName, std::string password, std::string email) = 0;
	virtual bool checkLoginInfo(std::string userName, std::string passwordToCheck) = 0;
};

