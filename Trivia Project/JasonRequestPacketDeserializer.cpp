#include "JasonRequestPacketDeserializer.h"
#include <iostream>
#include <string>
using json = nlohmann::json;

LoginRequest JasonRequestPacketDeserializer::deserializeLoginRequest(std::vector<char> buffer) 
{
	LoginRequest log;
	std::string str(buffer.begin(), buffer.end()-1);
	json data = json::parse(str);
	log.username = data["username"].get<std::string>();
	log.password = data["password"].get<std::string>();
	return log;
}

SignUpRequest JasonRequestPacketDeserializer::deserializeSignUpRequest(std::vector<char> buffer)
{
	SignUpRequest siReq;
	std::string str(buffer.begin(), buffer.end());
	json data = json::parse(str);
	siReq.username = data["username"].get<std::string>();
	siReq.password = data["password"].get<std::string>();
	siReq.email = data["email"].get<std::string>();
	return siReq;
}
