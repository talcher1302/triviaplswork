#include "RequestHandelerFactory.h"
#include "LoginRequestHandeler.h"

RequestHandelerFactory::RequestHandelerFactory(IDatabase* dataBase) : m_loginManager(dataBase)
{
}

LoginRequestHandeler* RequestHandelerFactory::createLoginRequestHandeler()
{
	return new LoginRequestHandeler(this, &m_loginManager);
} 