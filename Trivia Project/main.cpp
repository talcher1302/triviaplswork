#include "Communicator.h"
#include "DataBase.h"
#include "RequestHandelerFactory.h"
#include "sqlite3.h"
#include "Server.h"
int main()
{
	DataBase* db = new DataBase();
	if (!db->open())
	{
		std::cout << "there was an error while opening the database..." << std::endl;
	}
	else
	{
		Server server(db);
		server.run();
	}
	system("pause");
	return 0;
}