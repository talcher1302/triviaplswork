#include "LoginManager.h"
#include "DataBase.h"
#include <exception>

LoginManager::LoginManager(IDatabase* database)
{
	m_database = database;
}

void LoginManager::signUp(std::string username, std::string password, std::string email)
{
	if (m_database->doesUserExists(username))
	{
		throw std::exception("username already in use!");
	}
	m_database->addUser(username, password, email);
	login(username, password);
}

bool LoginManager::login(std::string username, std::string password)
{
	for (auto user : m_loggedUsers)//check if user exists
	{
		if (username == user.getUsername())
			return false; 
	}
	if (!m_database->checkLoginInfo(username, password))
	{
		return false;
	}
	LoggedUser user(username);
	m_loggedUsers.push_back(user);
	return true;
}

void LoginManager::logout(LoggedUser userToLogOut)
{
	m_loggedUsers.erase(std::remove(m_loggedUsers.begin(), m_loggedUsers.end(), userToLogOut), m_loggedUsers.end());
}

