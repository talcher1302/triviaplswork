#include "DataBase.h"

//static int checkUser(void *data, int argc, char **argv, char **azColName);

DataBase::DataBase()
{
}


DataBase::~DataBase()
{
}
 
bool DataBase::open()
{
	std::string dbFileName = "TriviaDataBase.sqlite";
	int res = sqlite3_open(dbFileName.c_str(), &_db);
	char *errMessage = nullptr;
	std::string sqlStatement = "CREATE TABLE IF NOT EXISTS 'USERS' ('username' TEXT NOT NULL, 'password' TEXT NOT NULL, 'email' TEXT NOT NULL, PRIMARY KEY('username'));";
	res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if(res == SQLITE_OK)
		return true;
	return false;
}

void DataBase::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}

bool DataBase::doesUserExists(std::string userName)
{
	char** errMessage = nullptr;
	std::string sqlStatement = "SELECT * FROM USERS WHERE NAME LIKE '" + userName +"';";
	std::string ans = "good";
	sqlite3_exec(_db, sqlStatement.c_str(), checkUser, &ans, errMessage);
	return ans == "ERROR";
}

int DataBase::checkUser(void *data, int argc, char **argv, char **azColName)
{
	if (argc != 0)
		*((std::string*)data) = "ERROR";
	return 0;
}

void DataBase::addUser(std::string userName, std::string password, std::string email)
{
	char** errMessage = nullptr;
	std::string sqlStatement = "insert into users (username, password, email) values ('" + userName + "', '" + password + "', '" + email + "');";
	sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, errMessage);
}

bool DataBase::checkLoginInfo(std::string userName, std::string passwordToCheck)
{
	char** errMessage = nullptr;
	std::string password = "";
	std::string sqlStatement = "select password from users where username like '" + userName +"';";
	sqlite3_exec(_db, sqlStatement.c_str(), checkPassword, &password, errMessage);
	return password == passwordToCheck;
}

int DataBase::checkPassword(void *data, int argc, char **argv, char **azColName)
{
	if(argc == 1)
		*((std::string*)data) = argv[0];
	return 0;
}