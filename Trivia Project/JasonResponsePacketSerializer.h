#pragma once
#include <iostream>
#include <vector>
#include "errorResponse.h"
#include "LoginResponse.h"
#include "SignUpResponse.h"


class JasonResponsePacketSerializer
{
public:
	static std::vector<char> serializeResponse(errorResponse err);
	static std::vector<char> serializeResponse(LoginResponse res);
	static std::vector<char> serializeResponse(SignUpResponse res);
}; 


