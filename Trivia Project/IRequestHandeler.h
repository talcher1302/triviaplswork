#pragma once
#include <iostream>
#include "RequestHandelerFactory.h"
#include "LoginResponse.h"
#include "SignUpResponse.h"
#include "LoginRequest.h"
#include "signUpRequest.h"

extern class RequestHandelerFactory;

typedef struct Request
{
	unsigned int RequestId;
	time_t receivalTime;
	std::vector <char> buffer; 
} Request;

typedef struct RequestResult RequestResult;

class IRequestHandeler
{
public:
	virtual bool isRequestRelevant(Request req) = 0; 
	virtual RequestResult handleRequest(Request req) = 0;
};

typedef struct RequestResult
{
	std::vector<char> response;
	IRequestHandeler* newHandeler;
} RequestResult;