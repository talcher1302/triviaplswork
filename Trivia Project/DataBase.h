#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
class DataBase : public IDatabase
{
public:
	DataBase();
	~DataBase();
	bool open(); 
	void close();	
	
	//
	virtual bool checkLoginInfo(std::string userName, std::string passwordToCheck);
	virtual bool doesUserExists(std::string userName);
	virtual void addUser(std::string userName, std::string password, std::string email);
	//static functions
	static int checkUser(void *data, int argc, char **argv, char **azColName);
	static int checkPassword(void *data, int argc, char **argv, char **azColName);

private:
	sqlite3* _db;
};