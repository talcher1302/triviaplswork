#pragma once
#include <iostream>
#include "LoginRequest.h"
#include "signUpRequest.h"
#include <vector>
#include "json.hpp"

class JasonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<char> buffer);
	static SignUpRequest deserializeSignUpRequest(std::vector<char> buffer);
}; 

