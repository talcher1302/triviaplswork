#include "JasonResponsePacketSerializer.h"
#include "json.hpp"
using json = nlohmann::json;

#define ERROR_CODE '0'
#define LOGIN_CODE '1'
#define SIGN_UP_CODE '2'

std::vector<char> JasonResponsePacketSerializer::serializeResponse(errorResponse err) 
{
	json j;
	j["message"] = err.message;
	char code = ERROR_CODE;
	std::string serializedJson = j.dump();
	std::string buffer = (code + std::to_string(serializedJson.length()) + serializedJson);
	std::vector<char> v;
	std::cout << buffer << std::endl;
	std::copy(buffer.begin(), buffer.end(), std::back_inserter(v));
	return v;
}

std::vector<char> JasonResponsePacketSerializer::serializeResponse(LoginResponse res)
{
	json j;
	j["status"] = res.status;
	char code = LOGIN_CODE;
	std::string serializedJson = j.dump();
	std::string buffer = (code + std::to_string(serializedJson.length()) + serializedJson);
	std::cout << buffer << std::endl;
	std::vector<char> v(buffer.begin(), buffer.end());
	return v;
}

std::vector<char> JasonResponsePacketSerializer::serializeResponse(SignUpResponse res)
{
	json j;
	j["status"] = res.status;
	char code = SIGN_UP_CODE;
	std::string serializedJson = j.dump();
	std::string buffer = (code + std::to_string(serializedJson.length()) + serializedJson);
	std::vector<char> v;
	std::cout << buffer << std::endl;
	std::copy(buffer.begin(), buffer.end(), std::back_inserter(v));
	return v;
}
