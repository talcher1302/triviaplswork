#pragma once
#include "LoginManager.h"

extern class LoginRequestHandeler;

class RequestHandelerFactory
{
private:
	LoginManager m_loginManager;
public:
	RequestHandelerFactory(IDatabase* dataBase);
	LoginRequestHandeler* createLoginRequestHandeler();
}; 

