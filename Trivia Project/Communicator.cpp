#pragma once
#pragma warning(disable : 4996)
#include "Communicator.h"
#include "IRequestHandeler.h"
#include <ctime>
#include "JasonRequestPacketDeserializer.h"
#include "JasonResponsePacketSerializer.h"
#include <sstream>
#include <bitset>
char* convertorFromBin(std::string bin, int size);
char* convertorFromString(std::vector<char> text, int size);
char  binToChar(const std::string& in);
std::string charToBin(char ch);

int it = 0;

void Communicator::startThreadForNewClient(SOCKET socket)
{
	it++;
	std::cout <<".............." << it << ".............." << std::endl;
	std::thread(&Communicator::handleRequests, this, socket).detach();
}

Communicator::Communicator(RequestHandelerFactory* RHF) : m_handlerFactory(RHF)
{
	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
	{
		throw std::exception("wsa startup failed");
	}
	std::cout << "Starting..." << std::endl;
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try 
	{
		closesocket(_serverSocket);
	}

	catch (...){}
	try
	{
		WSACleanup();
	}
	catch (...) {}
}

void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	std::cout << "# Binding..." << std::endl;
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	std::cout << "# Listening..." << std::endl;
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "# Server is running..." << std::endl;
	while (true)
	{
		accept();
	}
}

void Communicator::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client connected" << std::endl;

	
	m_clients[client_socket] = (IRequestHandeler*)m_handlerFactory->createLoginRequestHandeler();
	startThreadForNewClient(client_socket);
}

void Communicator::handleRequests(SOCKET socket)
{
	Request req;
	RequestResult res;
	unsigned int size = 0;
	int len = 0;
	try {
		while (true)
		{
			char* id = new char[8];
			char* length = new char[16];
			//get the time
			req.receivalTime = std::time(0);
			//get the id
			int check = recv(socket, id, 8, 0);
			if (check == SOCKET_ERROR || check == 0)
			{
				closesocket(socket);
				break;
			}
			char* id2 = convertorFromBin(id, 1);
			req.RequestId = (id2[0] - 48);
			//get the length
			check = recv(socket, length, 16, 0);
			if (check == SOCKET_ERROR || check == 0)
			{
				closesocket(socket);
				break;
			}
			char* length2 = convertorFromBin(length, 2);
			len = (length2[0] - 48);
			len = len*10 + (length2[1] - 48);
			len *= 8;
			char* buff = new char[len + sizeof(char)];
			//get the rest of the message
			check = recv(socket, buff, len, 0);
			if (check == SOCKET_ERROR || check == 0)
			{
				closesocket(socket);
				break;
			}
			char* buffe = convertorFromBin(buff, len/8);
			std::vector<char>buffer(buffe, buffe + len);
			req.buffer = buffer;
			try
			{
				if (!m_clients[socket]->isRequestRelevant(req))
				{
					throw std::exception("irrelevant request");
				}
				res = m_clients[socket]->handleRequest(req);
				delete m_clients[socket];
			}
			catch (const std::exception &e)
			{
				//return error response, and create new handler (delete all the progress that have benn made)
				errorResponse erresp;
				erresp.message = e.what();
				std::cerr << e.what() << std::endl;
				res.response = JasonResponsePacketSerializer::serializeResponse(erresp);
				res.newHandeler = m_clients[socket];
			}
			m_clients[socket] = res.newHandeler;
			char* messageForClient = convertorFromString(res.response, res.response.size());
			if(send(socket, messageForClient, strlen(messageForClient), 0) == SOCKET_ERROR)
			{				
				wchar_t *s = NULL;
				FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, WSAGetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPWSTR)&s, 0, NULL);
				std::cerr << "ERROR: " << s << std::endl;
				LocalFree(s);
			}
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
	std::cout << "\n connection lost with the client...\n";
}

char* convertorFromBin(std::string bin, int size)
{
	std::string output;
	std::string letter;
	int k = 0;
	for(int i = 0; i < size; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			letter += bin[k + j];
		}
		k += 8;
		output += binToChar(letter);
		letter.clear();
	}
	char* ans = new char[output.length() + 1];
	strcpy(ans, output.c_str());
	return ans;
}

char* convertorFromString(std::vector<char> text, int size)
{
	std::string output;
	std::string letter;
	std::string str(text.begin(), text.end());
	for (int i = 0; i < size; i++)
	{
		letter = charToBin(str[i]);
		output += letter;
		letter.clear();
	}
	char* ans = new char[output.length() + 1];
	strcpy(ans, output.c_str());
	return ans;

}

char binToChar(const std::string& in)
{
	char temp = 0;
	for (int i = 0; i < 8; i++)
	{
		if ('1' == in.at(i)) {
			temp += pow(2, 7 - i);
		}
	}
	return temp;
}

std::string charToBin(char ch)
{
	int i = CHAR_BIT;
	std::string letter;
	while (i > 0)
	{
		--i;
		letter += (ch&(1 << i) ? '1' : '0');
	}
	return letter;
}
