#pragma once
#include "IDatabase.h"
#include <vector>
#include "LoggedUser.h"

class LoginManager
{
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers; 
public:
	LoginManager(IDatabase* database);
	void signUp(std::string username, std::string password, std::string email);
	bool login(std::string username, std::string password);
	void logout(LoggedUser user);
};

