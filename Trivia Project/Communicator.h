#pragma once
#pragma comment (lib, "ws2_32.lib")

#include "LoginResponse.h"
#include "SignUpResponse.h"
#include "LoginRequestHandeler.h"
#include "IRequestHandeler.h"
#include <map>
#include <WinSock2.h>
#include "RequestHandelerFactory.h"
#include <thread>
#include "WSAInitializer.h"
class Communicator
{
private:
	std::map<SOCKET, IRequestHandeler*> m_clients;
	RequestHandelerFactory* m_handlerFactory;
	SOCKET _serverSocket;
	void startThreadForNewClient(SOCKET socket);
	void accept();
public: 
	Communicator(RequestHandelerFactory* RHF);
	~Communicator();
	void bindAndListen(int port);
	void handleRequests(SOCKET socket);

};