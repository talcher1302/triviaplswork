#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "IDatabase.h"
#include "RequestHandelerFactory.h"
#include "Communicator.h" 

extern class Communicator;

class Server
{
public:
	void run();
	Server(IDatabase* m_database);

private:
	RequestHandelerFactory m_handlerFactory;
	Communicator m_comunicator;
	IDatabase* m_database;
};

